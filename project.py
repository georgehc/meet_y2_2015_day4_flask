from flask import Flask
from flask import request
from flask import render_template
app = Flask(__name__)

# SQLAlchemy stuff
from crud import Base, Person
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
engine = create_engine('sqlite:///crudlab.db')
Base.metadata.bind = engine
DBSession = sessionmaker(bind=engine)
session = DBSession()


@app.route('/')
def view_my_friends():
    friends = []  # CHANGE THIS TO READ ALL FRIENDS FROM THE DATABASE
    return render_template('view_my_friends.html', friends=friends)


@app.route('/add', methods=['GET', 'POST'])
def add_friend():
    if request.method == 'POST':
        # CHANGE THIS TO ACTUALLY CREATE A NEW FRIEND IN THE DATABASE
        return "NOT YET IMPLEMENTED"
    else:
        return render_template('add_friend.html')


@app.route('/BROKEN-ROUTE-FOR-YOU-TO-FIX', methods=['GET', 'POST'])
def edit_friend(person_id):
    friend = None  # CHANGE THIS TO BE THE CORRECT FRIEND FROM THE DATABASE
    if request.method == 'POST':
        # CHANGE THIS TO ACTUALLY EDIT THE FRIEND AND UPDATE THE DATABASE
        return "NOT YET IMPLEMENTED"
    else:
        return render_template('edit_friend.html', friend=friend)


@app.route('/BROKEN-ROUTE-FOR-YOU-TO-FIX', methods=['GET', 'POST'])
def delete_friend(person_id):
    friend = None  # CHANGE THIS TO BE THE CORRECT FRIEND FROM THE DATABASE
    if request.method == 'POST':
        # CHANGE THIS TO ACTUALLY DELETE THE FRIEND FROM THE DATABASE
        return "NOT YET IMPLEMENTED"
    else:
        return render_template('delete_friend.html', friend=friend)


if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0', port=5000)
