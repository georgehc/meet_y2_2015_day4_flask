from sqlalchemy import Column, Date, Float, ForeignKey, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker
from sqlalchemy import create_engine

import datetime

Base = declarative_base()

class Person(Base):
    __tablename__ = 'person'
    name = Column(String(60))
    id = Column(Integer, primary_key=True)
    gender = Column(String(15))
    nationality = Column(String(30))
    hometown = Column(String(60))  # city/town name

if __name__ == '__main__':
    engine = create_engine('sqlite:///crudlab.db')
    Base.metadata.create_all(engine)
    Base.metadata.bind = engine
    DBSession = sessionmaker(bind=engine)
    session = DBSession()

    ### These are the commands you just saw live.

    # This queries the Person table
    session.query(Person).all()

    lorenzo = Person(
            name = 'Lorenzo Brown',
            gender = 'male',
            nationality = 'American',
            hometown = 'San Francisco')

    michele = Person(
            name = 'Michele Pratusevich',
            gender = 'female',
            nationality = 'American',
            hometown = 'Boston')

    omri = Person(
            name = 'Omri Doron',
            gender = 'male',
            nationality = 'Israeli',
            hometown = 'Jerusalem')

    session.add(lorenzo)
    session.add(michele)
    session.add(omri)
    session.commit()
